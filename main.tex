%% For double-blind review submission, w/o CCS and ACM Reference (max submission space)
% \documentclass[acmlarge]{acmart}%\settopmatter{printfolios=false,printccs=false,printacmref=false}
%% For double-blind review submission, w/ CCS and ACM Reference
\documentclass[acmsmall]{acmart}\settopmatter{printfolios=true}
%% For single-blind review submission, w/o CCS and ACM Reference (max submission space)
%\documentclass[acmsmall,review]{acmart}\settopmatter{printfolios=true,printccs=false,printacmref=false}
%% For single-blind review submission, w/ CCS and ACM Reference
%\documentclass[acmsmall,review]{acmart}\settopmatter{printfolios=true}
%% For final camera-ready submission, w/ required CCS and ACM Reference
%\documentclass[acmsmall]{acmart}\settopmatter{}


%% Journal information
%% Supplied to authors by publisher for camera-ready submission;
%% use defaults for review submission.
% \acmJournal{PACMPL}
% \acmVolume{1}
% \acmNumber{CONF} % CONF = POPL or ICFP or OOPSLA
% \acmArticle{1}
% \acmYear{2018}
% \acmMonth{1}
% \acmDOI{} % \acmDOI{10.1145/nnnnnnn.nnnnnnn}
% \startPage{1}

%% Copyright information
%% Supplied to authors (based on authors' rights management selection;
%% see authors.acm.org) by publisher for camera-ready submission;
%% use 'none' for review submission.
\setcopyright{none}
%\setcopyright{acmcopyright}
%\setcopyright{acmlicensed}
%\setcopyright{rightsretained}
%\copyrightyear{2018}           %% If different from \acmYear

%% Bibliography style
\bibliographystyle{ACM-Reference-Format}
%% Citation style
%% Note: author/year citations are required for papers published as an
%% issue of PACMPL.
\citestyle{acmauthoryear}   %% For author/year citations


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Note: Authors migrating a paper from PACMPL format to traditional
%% SIGPLAN proceedings format must update the '\documentclass' and
%% topmatter commands above; see 'acmart-sigplanproc-template.tex'.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Some recommended packages.
\usepackage{booktabs}   %% For formal tables:
                        %% http://ctan.org/pkg/booktabs
\usepackage{subcaption} %% For complex figures with subfigures/subcaptions
                        %% http://ctan.org/pkg/subcaption
\input{include}
\input{macro}

\AtBeginDocument{%
  \providecommand\BibTeX{{%
    \normalfont B\kern-0.5em{\scshape i\kern-0.25em b}\kern-0.8em\TeX}}}


\begin{document}

%% Title information
\title{Mechanizing Normalization Proof for Layered System F}         %% [Short Title] is optional;
                                        %% when present, will be used in
                                        %% header instead of Full Title.
% \titlenote{with title note}             %% \titlenote is optional;
%                                         %% can be repeated if necessary;
%                                         %% contents suppressed with 'anonymous'
% \subtitle{Where Meta-programming Meets Intensional Analysis}                     %% \subtitle is optional
% \subtitlenote{with subtitle note}       %% \subtitlenote is optional;
%                                         %% can be repeated if necessary;
%                                         %% contents suppressed with 'anonymous'


%% Author information
%% Contents and number of authors suppressed with 'anonymous'.
%% Each author should be introduced by \author, followed by
%% \authornote (optional), \orcid (optional), \affiliation, and
%% \email.
%% An author may have multiple affiliations and/or emails; repeat the
%% appropriate command.
%% Many elements are not rendered, but should be provided for metadata
%% extraction tools.

%% Author with single affiliation.
\author{Jason Z. S. Hu}
\email{zhong.s.hu@mail.mcgill.ca}
\affiliation{%
  \department{School of Computer Science}
  \institution{McGill University}
  \streetaddress{McConnell Engineering Bldg. Room 225, 3480 University St.}
  \city{Montr\'eal}
  \state{Qu\'ebec}
  \country{Canada}
  \postcode{H3A 0E9}
}


%% Abstract
%% Note: \begin{abstract}...\end{abstract} environment must come
%% before \maketitle command
% \begin{abstract}
%   \input{abstract}
% \end{abstract}


%% 2012 ACM Computing Classification System (CSS) concepts
%% Generate at 'http://dl.acm.org/ccs/ccs.cfm'.
% \begin{CCSXML}
% <ccs2012>
% <concept>
% <concept_id>10011007.10011006.10011008</concept_id>
% <concept_desc>Software and its engineering~General programming languages</concept_desc>
% <concept_significance>500</concept_significance>
% </concept>
% <concept>
% <concept_id>10003456.10003457.10003521.10003525</concept_id>
% <concept_desc>Social and professional topics~History of programming languages</concept_desc>
% <concept_significance>300</concept_significance>
% </concept>
% </ccs2012>
% \end{CCSXML}

% \ccsdesc[500]{Software and its engineering~General programming languages}
% \ccsdesc[300]{Social and professional topics~History of programming languages}
%% End of generated code


%% Keywords
%% comma separated list
\keywords{modal type theory, contextual types, meta-programming, normalization} %% \keywords are mandatory in final camera-ready submission


%% \maketitle
%% Note: \maketitle command must come after title commands, author
%% commands, abstract environment, Computing Classification System
%% environment and commands, and keywords command.
\maketitle

\section{Background}

\newcommand{\tsize}{\texttt{size}}

In our recent layered model type theory~\citep{hu2023layered}, we have developed a
modal simple type theory, 2LC\lambox, in which we can do pattern matching on code. %
In 2LC\lambox, we introduce \emph{layers} in the typing judgment, so that the type
theory effectively is separated into two languages: simply typed lambda calculus
(STLC) at layer 0 and its meta-language at layer 1 extended with contextual
types~\citep{nanevski_contextual_2008}. %
At layer 1, we are able to perform pattern matching on the code of STLC at layer 0,
while at layer 0, we cannot do meta-programming at all. %
In this way, we achieve \emph{covering} pattern matching on all code, which is a
crucial property that leads to the eventual normalization proof.

Nevertheless, 2LC\lambox is not completely satisfactory, as it currently does not
support general \emph{recursion} on code. %
Consider the following function which computes the size of the AST of a given code
(i.e. $x$):
\begin{align*}
  \tsize := \lambda x. \matc x \sep \lambda y. ?t \STo 1 + \tsize~(\boxit t) \sep \cdots
\end{align*}
We compute the size by pattern matching on $x$ and analyze each case. %
Here, we focus on the case for $\lambda$ and elide all other cases. %
In this case, we use $?t$ to capture the body of the $\lambda$ expression. %
The idea is to perform a recursive call on $t$ and increment the result by one. %
However, this function cannot be type-checked in 2LC\lambox, because
\begin{itemize}
\item the type of the scrutinee has changed, and
\item the local context of the recursive call has been extended.
\end{itemize}
More concretely, let us assume $x$ has type $\cont {S \func T}$ for some local context
$\Gamma$. %
We are sure that $x$ has a function type because it has a case for $\lambda$. %
However, if we check the type of $\boxit t$, it is $\cont[\Gamma, y : S] T$. %
Since both the context and the type fail to match, a recursion on $\boxit t$ will not
work. %
To enable recursion on code, two features must be introduced:
\begin{itemize}
\item contextual variables, which allow abstraction over local contexts, and
\item parametricity, which allows the type of the code to vary. 
\end{itemize}
Parametricity can be achieved by either System F's type variables, or universes in
dependent type theory. %
In this proposal, we consider the possibility to extend System F with layers and
contextual variables to enable recursion on code. %

This effort is expected to make the following contributions:
\begin{itemize}
\item We add the power of \emph{general intensional recursion} to System F. %
  In this work, we aim at the coverage property of pattern matching on code, and hence
  this contribution is distinguished from Moebius~\citep{Jang:POPL22}. %
  On a theoretical side, coverage paves ways to a normalization proof; on a practical
  side, it implies an algorithm to check coverage of pattern matching on code in an
  implementation to help (meta-)programmers to detect bugs earlier. 

\item We prove the normalization of layered System F. %
  Achieving coverage of pattern matching immediately unblocks the normalization
  proof. %
  The normalization proof ensures the logical consistency of the system and therefore
  justifies the system from a logical and type theoretic point of view.
  
\item Layered System F will serves as a foundation for implementing a practical
  programming language that supports intensional recursion. 
\end{itemize}

\section{A Possible Direction}

Since System F already supports type variables, we only need to add a
\emph{meta-function space} to the language to support abstraction over contextual
variables. %
Consider the $\tsize$ example above, we implement it as follows in layered System
F:
\begin{align*}
  \tsize &: (g :: \Ctx) (\alpha :: (\judge[g]*)) \To \cont[g]{\alpha} \to \Nat
  \\
  \tsize &:= \Lambda g~\alpha. \lambda x. \matc x \sep \lambda y.?t \STo 1 +
           \tsize~(\boxit t) \sep \cdots
\end{align*}
Let us first take a look at its type. %
As we have mentioned, a meta-function space is introduced, denoted by $\To$. %
There are two kinds of arguments can be abstracted by a meta-function: contextual
variables ($g$ in the example), and open types parameterized by contextual variables
(i.e. $\alpha$). %
Then the rest is just what has been described above. %
However, now we have parameterized both the local context and the type of code, so we
can finally type-check this program. %
In the $\lambda$ case, we know that $x$ must have type $\cont[g]{\beta \func \beta'}$
for some $\beta$ and $\beta'$. %
$\beta \func \beta'$ is then unified with $\alpha$. %
The recursive call takes an argument of type $\cont[g, y: \beta]{\beta'}$. %
Since $\tsize$ is parameterized, we simply let $(g,y:\beta)/g$ and $\beta'/\alpha$ and
the recursive call now type-checks.

Under the hood, it is more convenient to reason about recursion using an elimination
principle. %
In general, for some motive $M$ that has $g$ and $\alpha$ open, we have the following
principle:
\begin{mathpar}
  \inferrule
  {\ltyping[\Psi, g : \Ctx, \beta : (\judge[g]*), \beta' : (\judge[g]*), u : (\judge[g, y :
    \beta]{\beta'})][\Gamma, x : M[(g,y:\beta)/g, \beta'/\alpha]] 1
    {t_\lambda}{M[\beta \func \beta'/\alpha]} \\
    \cdots \\
    \judge[\Psi] \Delta \\ \ltyping[\Psi][\Delta] 1 T * \\ \ltyping 1 t {\cont[\Delta]
    T}}
  {\ltyping 1 {\elim M~{(g~\beta~\beta'~u~x. t_{\lambda})} \cdots ~ \Delta ~ T ~ t}{M[\Delta/g, T/\alpha]}}
\end{mathpar}
We only focus on the $\lambda$ case. %
This case takes five arguments. %
They abstract the local context ($g$), the input type ($\beta$), the output type
($\beta'$), the body of $\lambda$ ($u$) and the recursive call (or inductive
hypothesis, $x$), respectively. %
Note that they are extended to different contexts. %
The unification is implicit here as $T$ will have to be $\beta \func \beta'$ when
hitting the $\lambda$ case. %
It is clearer when we consider the reduction rule:
\begin{align*}
  \Psi; \Gamma \vdash_1 &~ {\elim M~{(g~\beta~\beta'~u~x. t_{\lambda})} \cdots ~ \Delta ~ (S \func
                         T) ~ (\boxit{(\lambda y. t)})} \\
  \approx &~ t_{\lambda}[\Delta/g, S/\beta, T/\beta', t/u, (\elim
            M~{(g~\beta~\beta'~u~x. t_{\lambda})} \cdots ~ (\Delta, y : S) ~ T ~
            (\boxit t)/x] \\
  : &~ M[\Delta/g, S \func T/\alpha]
\end{align*}
On the left hand side, we eliminate the code of a $\lambda$ abstraction,
$\boxit{(\lambda y. t)}$. %
Note that how $x$ is substituted by the recursive call on the right hand side. %
In this way, we do not need to be concerned about unification, which enable a more tractable
mechanization in a purely type-theoretic fashion.

Regarding the normalization technique, we can either follow
\citet{abel_normalization_2013,DBLP:conf/lpar/Abel08} to establish a normalization by
evaluation proof, or use more conventional Girard's reducibility candidates. %
We suppose that the latter approach is easier to achieve the result, despite having
more verbose technicality. %
Normalization also enables a subsequent study of equational theory, which provides a
foundation for different reduction strategies in actual implementations. 

\section{Comparison with Moebius}

Closely related to the proposed system is Moebius~\citep{Jang:POPL22}, which also
extends System F with pattern matching on code. %
The critical difference between this effort and Moebius is that Moebius does not
ensure the coverage of pattern matching on code, and it is not even obvious what is a
sound predicate for coverage. %
This disadvantage subsequently prevents normalization of Moebius.

This effort starts from a more type-theoretic angle. %
Coverage and normalization are targeted from the beginning so the logical
justification of layered System F is particularly strong. %
Moreover, a 2-layered version of System F is simpler than Moebius and is realistic for
a prototype implementation or extensions to existing languages due to the
comparatively simple context management. %
On a broader and more practical side, normalization can always be lifted when the
system is used as a programming language, but coverage checking derived from this
development will still substantially help (meta-)programmers to catch bugs earlier in
the development stage.

\section{Workload And Plan}

This proposal describes a difficult research project with potential technical difficulties
that are not predictable at the current stage. %
Therefore, we suggest to have one student to consistently work on this problem for a
continuous investigation. %
This project is expected to carry out for around two years. %
A potential research plan is as follows:
\begin{enumerate}
\item Mechanize a normalization proof of System F in Coq. %
  This step could take 4 months of full-time development. %
  The purpose of this step is for the student to learn about the normalization problem
  of System F and understand the impact of impredicativity in normalization. %

\item Extend System F with $\tletbox$, contextual variables and contextual types. %
  This also means that we add meta-functions to the language. %
  Basically we start with a simpler eliminator to explore the semantics. %
  This step could take up to one year to complete. %
  The purpose of this step is to discover a semantic model that fits impredicativity
  and code together.
  
\item Extend the system in the previous step with a eliminator as discussed above. %
  This step is the final step and accomplishes the goal. %
  This step could take 6 months. %
  It takes shorter time than the previous one as the previous step already establishes
  a suitable model, so what is left in this step is to resolve the remaining technical
  details to support intensional recursion. %
  
\item If time permitted, a prototype implementation is something good to try. 
\end{enumerate}


\bibliography{ref}

\appendix

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:

