.PHONY: all clean

TARGETS::=main.pdf syllabus.pdf comp_report.pdf template.pdf
TOREMOVE::=$(shell cat .gitignore)
TEXFILES::=$(subst ./,,$(shell find . -name '*.tex'))

all:main.pdf

${TARGETS}: %.pdf: %.tex $(filter-out $(subst .pdf,.tex,${TARGETS}),${TEXFILES})
	pdflatex -shell-escape -halt-on-error $(@:.pdf=)
	bibtex $(@:.pdf=)
	pdflatex -shell-escape -halt-on-error $(@:.pdf=)
	pdflatex -shell-escape -halt-on-error $(@:.pdf=)

temp.pdf: temp.tex
	bibtex $(@:.pdf=) || true
	pdflatex -shell-escape -halt-on-error $(@:.pdf=)

FINDCMDPRE::=${foreach x, ${TOREMOVE}, -o -name '$x'}
FINDCMD::=${wordlist 2,${words ${FINDCMDPRE}},${FINDCMDPRE}}

clean:
	find . \( ${FINDCMD} \) -exec rm {} +
	find . -name '_minted*' -exec rm -r {} +
	rm main.pdf
